# Hello API

[ ![Codeship Status for zdne/helloapi](https://codeship.com/projects/24265230-b2f0-0133-8b0b-567510b4e5ac/status?branch=master)](https://codeship.com/projects/133514)

This repository demonstrates one-way integration of Bitbucket and Bitbucket
server (fromerly known as Stash) with Apiary using the Codeship CI.

See the [Using Apiary with Bitbucket](https://docs.apiary.io/api_101/bitbucket-integration/)
article.

[API interactive documentation](http://docs.hellobitbucket.apiary.io/)

## License

MIT.
